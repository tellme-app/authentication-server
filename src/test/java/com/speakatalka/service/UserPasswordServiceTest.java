//package com.speakatalka.service;
//
//import com.speakatalka.entity.User;
//import com.speakatalka.repository.UserRepository;
//import io.micronaut.http.HttpStatus;
//import io.micronaut.http.exceptions.HttpStatusException;
//import io.micronaut.security.authentication.AuthenticationRequest;
//import io.micronaut.security.authentication.UsernamePasswordCredentials;
//import io.micronaut.test.annotation.MicronautTest;
//import io.micronaut.test.annotation.MockBean;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mindrot.jbcrypt.BCrypt;
//
//import javax.inject.Inject;
//import java.util.List;
//import java.util.Optional;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertThrows;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.when;
//
//@MicronautTest
//class UserPasswordServiceTest {
//
//    UserPasswordService userPasswordService;
//    Optional<User> successUserOptional = Optional.of(User.builder()
//            .email("success@success.ru")
//            .fio("Success Success")
//            .name("Success1")
//            .id(1)
//            .password(BCrypt.hashpw("success", BCrypt.gensalt()))
//            .build());
//    Optional<User> notFoundUser = Optional.empty();
//    Optional<User> passwordIsWrong = Optional.of(User.builder()
//            .email("notSuccessful@notSuccessful.ru")
//            .fio("Not Successful")
//            .name("NotSuccessful666")
//            .id(666)
//            .password(BCrypt.hashpw("notSuccessful", BCrypt.gensalt()))
//            .build());
//
//    @BeforeEach
//    void setUp() {
//        userPasswordService = UserPasswordService.builder().userRepository(userRepository).build();
//    }
//
//    @Test
//    public void getUserDetails_successful() {
//        AuthenticationRequest<?, ?> authenticationRequest = new UsernamePasswordCredentials("Success1", "success");
//        var userDetails = userPasswordService.getUserDetails(authenticationRequest);
//        assertEquals("Success1", userDetails.getUsername());
//        assertEquals(1, userDetails.getAttributes("roles", "userName").get("id"));
//        assertEquals("success@success.ru", userDetails.getAttributes("roles", "userName").get("userEmail"));
//        assertEquals("Success Success", userDetails.getAttributes("roles", "userName").get("userFio"));
//        assertEquals(List.of("ROLE_BASIC"), userDetails.getRoles());
//    }
//
//    @Test
//    public void getUserDetails_notSuccessful_userNotFound() {
//        AuthenticationRequest<?, ?> authenticationRequest = new UsernamePasswordCredentials("success", "success");
//        var exception = assertThrows(HttpStatusException.class, () -> userPasswordService.getUserDetails(authenticationRequest));
//        assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus());
//        assertEquals("Пользователь с таким именем не найден", exception.getMessage());
//    }
//
//    @Test
//    public void getUserDetails_notSuccessful_passwordIsWrong() {
//        AuthenticationRequest<?, ?> authenticationRequest = new UsernamePasswordCredentials("NotSuccessful666", "wrongPassword");
//        var exception = assertThrows(HttpStatusException.class, () -> userPasswordService.getUserDetails(authenticationRequest));
//        assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus());
//        assertEquals("Неверный пароль", exception.getMessage());
//    }
//
//}
