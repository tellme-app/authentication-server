package com.speakatalka.service;

import com.speakatalka.entity.User;
import com.speakatalka.entity.UserRolesEntity;
import com.speakatalka.repository.UserRepository;
import com.speakatalka.repository.UserRolesRepository;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import io.micronaut.security.authentication.UserDetails;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@MicronautTest
class UserDetailsServiceTest {

    @Inject
    UserRepository userRepository;
    @Inject
    UserRolesRepository userRolesRepository;
    UserDetailsService userDetailsService;

    @BeforeEach
    void setUp() {
        var user = User.builder().name("test").id(1).fio("test test test").email("test@test.ru").build();
        var userRoles = List.of(
                UserRolesEntity.builder().roleName("ROLE_ADMIN").userId(1).userRoleId(1).build(),
                UserRolesEntity.builder().roleName("ROLE_USER").userId(1).userRoleId(2).build(),
                UserRolesEntity.builder().roleName("ROLE_REDACTOR").userId(1).userRoleId(3).build()
        );
        when(userRepository.findByName("test")).then(inv -> Optional.of(user));
        when(userRepository.findByName("test2")).then(inv -> Optional.empty());
        when(userRolesRepository.findAllByUserId(1)).then(inv -> userRoles);
        userDetailsService = UserDetailsService.builder().userRepository(userRepository).userRolesRepository(userRolesRepository).build();
    }

    @Test
    void userService_getUserDetails(){
        var userName = "test";
        UserDetails userDetails = userDetailsService.getUserDetails(userName);
    }

    @Test
    void userService_getUserDetails_userName(){
        var userName = "test";
        UserDetails userDetails = userDetailsService.getUserDetails(userName);
        assertEquals("test", userDetails.getUsername());
    }

    @Test
    void userService_getUserDetails_userRoles(){
        var userName = "test";
        var userRoles = List.of("ROLE_ADMIN", "ROLE_USER", "ROLE_REDACTOR");
        UserDetails userDetails = userDetailsService.getUserDetails(userName);
        assertEquals(userRoles, userDetails.getRoles());
    }

    @Test
    void userService_getUserDetails_attributes_userFio(){
        var userName = "test";
        UserDetails userDetails = userDetailsService.getUserDetails(userName);
        var attributes = userDetails.getAttributes("roles", "username");
        assertEquals("test test test", attributes.get("userFio"));
    }

    @Test
    void userService_getUserDetails_attributes_userEmail(){
        var userName = "test";
        UserDetails userDetails = userDetailsService.getUserDetails(userName);
        var attributes = userDetails.getAttributes("roles", "username");
        assertEquals("test@test.ru", attributes.get("userEmail"));
    }

    @Test
    void userService_getUserDetails_attributes_id(){
        var userName = "test";
        UserDetails userDetails = userDetailsService.getUserDetails(userName);
        var attributes = userDetails.getAttributes("roles", "username");
        assertEquals(1, attributes.get("id"));
    }

    @Test
    void userService_getUserDetails_userNotFound(){
        var userName = "test2";
        HttpStatusException exception = assertThrows(HttpStatusException.class, () -> userDetailsService.getUserDetails(userName));
        assertEquals(HttpStatus.FORBIDDEN, exception.getStatus());
        assertEquals("Пользователь с таким именем не найден", exception.getMessage());
    }


    @MockBean(UserRepository.class)
    public UserRepository userRepository() {
        return mock(UserRepository.class);
    }

    @MockBean(UserRolesRepository.class)
    public UserRolesRepository userRolesRepository() {
        return mock(UserRolesRepository.class);
    }
}
