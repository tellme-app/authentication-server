package com.speakatalka.repository;

import com.speakatalka.entity.UserSocialMedia;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

import java.util.Optional;

@Repository
public interface UserSocialMediaRepository extends CrudRepository<UserSocialMedia, Integer> {

    Optional<UserSocialMedia> findBySocialMediaNameAndSocialMediaId(String socialMediaName, String socialMediaId);
}
