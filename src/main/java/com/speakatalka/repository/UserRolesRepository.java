package com.speakatalka.repository;

import com.speakatalka.entity.UserRolesEntity;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

import java.util.List;

@Repository
public interface UserRolesRepository extends CrudRepository<UserRolesEntity, Integer> {

    List<UserRolesEntity> findAllByUserId(Integer userId);
}
