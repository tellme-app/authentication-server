package com.speakatalka.repository;

import com.speakatalka.entity.RefreshTokenEntity;
import edu.umd.cs.findbugs.annotations.NonNull;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Repository
public interface RefreshTokenRepository extends CrudRepository<RefreshTokenEntity, Long> {


    @Transactional
    RefreshTokenEntity save(@NonNull @NotBlank String userName,
                            @NonNull @NotBlank String refreshToken,
                            @NonNull @NotNull Boolean isRevoked);

    Optional<RefreshTokenEntity> findByRefreshToken(@NonNull @NotBlank String refreshToken);

    long updateByUserName(@NonNull @NotBlank String userName,
                          @NonNull @NotNull Boolean isRevoked);
}
