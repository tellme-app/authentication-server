package com.speakatalka.controllers;

import com.speakatalka.model.RegisterNewUserModel;
import com.speakatalka.service.RegisterNewUserService;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import lombok.AllArgsConstructor;

import javax.validation.Valid;
import java.security.Principal;


@Controller
@AllArgsConstructor
@Secured(SecurityRule.IS_ANONYMOUS)
public class PublicController {

    private final RegisterNewUserService registerNewUserService;

    @Get()
    public Principal home(Principal principal){
        return principal;
    }

    @Post("/register")
    public void register(@Body @Valid RegisterNewUserModel user){
        registerNewUserService.saveNewUser(user);
    }


}
