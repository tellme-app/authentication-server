package com.speakatalka.controllers;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import lombok.AllArgsConstructor;

@Controller("/secured")
@AllArgsConstructor
@Secured(SecurityRule.IS_AUTHENTICATED)
public class SecuredController {

    @Get
    public String success() {
        return "Success";
    }
}
