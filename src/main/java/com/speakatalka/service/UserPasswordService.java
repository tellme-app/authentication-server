package com.speakatalka.service;

import io.micronaut.security.authentication.AuthenticationRequest;
import io.micronaut.security.authentication.UserDetails;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;

@Singleton
@RequiredArgsConstructor
@Builder
@Slf4j
public class UserPasswordService {

    private final UserDetailsService userDetailsService;

    public UserDetails getUserDetails(AuthenticationRequest<?, ?> authenticationRequest) {
        log.debug("User name is {}", authenticationRequest.getIdentity());
        var userName = (String) authenticationRequest.getIdentity();
        var password = (String) authenticationRequest.getSecret();
        return userDetailsService.getUserDetails(userName, password);
    }

}
