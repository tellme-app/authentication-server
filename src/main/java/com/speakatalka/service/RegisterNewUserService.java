package com.speakatalka.service;

import com.speakatalka.entity.User;
import com.speakatalka.entity.UserRolesEntity;
import com.speakatalka.model.RegisterNewUserModel;
import com.speakatalka.repository.UserRepository;
import com.speakatalka.repository.UserRolesRepository;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import lombok.RequiredArgsConstructor;
import org.mindrot.jbcrypt.BCrypt;

import javax.inject.Singleton;

@Singleton
@RequiredArgsConstructor
public class RegisterNewUserService {

    private final UserRepository userRepository;
    private final UserRolesRepository userRolesRepository;
    private final String PASSWORD_REGEXP = "^(.{0,7}|[^0-9]|[^A-Z]|[^a-z]|[a-zA-Z0-9]).{6,}$";

    public void saveNewUser(RegisterNewUserModel user) {
        checkUserBeforeSave(user);
        var hashedPwd = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
        user.setPassword(hashedPwd);
        var savedUser = userRepository.save(User.builder()
                .email(user.getEmail())
                .fio(user.getFio())
                .name(user.getName())
                .password(user.getPassword())
                .build()
        );
        userRolesRepository.save(UserRolesEntity.builder().userId(savedUser.getId()).roleName("ROLE_USER").build());
    }

    private void checkUserBeforeSave(RegisterNewUserModel user){
        if (!user.getPassword().matches(PASSWORD_REGEXP))
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Пароль не удовлетворяет условиям");
        if (!user.getPassword().equals(user.getConfirmPassword()))
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Пароли не совпадают");
        if (userRepository.findByName(user.getName()).isPresent())
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Пользователь с таким именем уже существует");
        if (userRepository.findByEmail(user.getEmail()).isPresent())
            throw new HttpStatusException(HttpStatus.BAD_REQUEST, "Пользователь с таким адресом уже существует");
    }
}
