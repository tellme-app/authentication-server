package com.speakatalka.service.security;

import com.speakatalka.entity.User;
import com.speakatalka.model.oAuth.OAuthUser;
import com.speakatalka.model.oAuth.OpenIdUser;
import com.speakatalka.model.oAuth.SocialMediaNetwork;
import com.speakatalka.repository.UserRepository;
import com.speakatalka.repository.UserSocialMediaRepository;
import com.speakatalka.service.RegisterNewOAuthUser;
import com.speakatalka.service.UserDetailsService;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import io.micronaut.security.authentication.UserDetails;
import io.micronaut.security.oauth2.endpoint.token.response.OpenIdClaims;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;

@Singleton
@RequiredArgsConstructor
public class UserExtractor {

    private final UserSocialMediaRepository userSocialMediaRepository;
    private final UserRepository userRepository;
    private final UserDetailsService userDetailsService;
    private final RegisterNewOAuthUser registerNewOAuthUser;

    public UserDetails getUserDetails(OpenIdClaims openIdClaims, SocialMediaNetwork smn) {
        return getUserDetails(OpenIdUser.builder()
                .id(openIdClaims.getSubject())
                .email(openIdClaims.getEmail())
                .name(openIdClaims.getName())
                .build(), smn);
    }

    public UserDetails getUserDetails(OAuthUser oAuthUser, SocialMediaNetwork smn) {
        var user = extractUser(oAuthUser, smn);
        return userDetailsService.getUserDetails(user.getName());
    }

    private User extractUser(OAuthUser user, SocialMediaNetwork smn) {
        var userSocialMedia = userSocialMediaRepository.findBySocialMediaNameAndSocialMediaId(smn.name(), user.getId());
        if (userSocialMedia.isPresent())
            return userRepository.findById(userSocialMedia.get().getUserId())
                    .orElseThrow(() -> new HttpStatusException(HttpStatus.NOT_FOUND, "Пользователь с таким id не найден"));
        return registerNewOAuthUser.register(user, smn);
    }

}
