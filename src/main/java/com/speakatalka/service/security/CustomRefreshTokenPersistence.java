package com.speakatalka.service.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.speakatalka.entity.RefreshTokenEntity;
import com.speakatalka.service.UserDetailsService;
import io.lettuce.core.api.StatefulRedisConnection;
import io.micronaut.runtime.event.annotation.EventListener;
import io.micronaut.security.authentication.UserDetails;
import io.micronaut.security.errors.IssuingAnAccessTokenErrorCode;
import io.micronaut.security.errors.OauthErrorResponseException;
import io.micronaut.security.token.event.RefreshTokenGeneratedEvent;
import io.micronaut.security.token.refresh.RefreshTokenPersistence;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;

import javax.inject.Singleton;
import java.util.Optional;

@Singleton
@RequiredArgsConstructor
@Slf4j
public class CustomRefreshTokenPersistence implements RefreshTokenPersistence {
    private final StatefulRedisConnection<String, String> connection;
    private final ObjectMapper objectMapper;
    private final UserDetailsService userDetailsService;

    @Override
    @EventListener
    public void persistToken(RefreshTokenGeneratedEvent event) {
        if (event != null &&
                event.getRefreshToken() != null &&
                event.getUserDetails() != null &&
                event.getUserDetails().getUsername() != null) {
            var commands = connection.sync();
            String payload = event.getRefreshToken();
            var value = RefreshTokenEntity.builder()
                    .userName(event.getUserDetails().getUsername())
                    .refreshToken(payload)
                    .isRevoked(Boolean.FALSE)
                    .build();
            var key = "refresh-token:" + event.getRefreshToken();
            try {
                commands.set(key, objectMapper.writeValueAsString(value));
            } catch (JsonProcessingException e) {
                log.error("User is {}, message is {}", event.getUserDetails().getUsername(), e.getMessage());
                e.printStackTrace();
            }
        }
    }

    @Override
    public Publisher<UserDetails> getUserDetails(String refreshToken) {
        return Flowable.create(emitter -> {
            var command = connection.sync();
            var key = "refresh-token:" + refreshToken;
            var rawData = command.get(key);
            var tokenData = Optional.ofNullable(objectMapper.readValue(rawData, RefreshTokenEntity.class));

            if (tokenData.isPresent()) {
                RefreshTokenEntity token = tokenData.get();
                if (token.getIsRevoked()) {
                    emitter.onError(new OauthErrorResponseException(IssuingAnAccessTokenErrorCode.INVALID_GRANT, "refresh token revoked", null));
                } else {
                    emitter.onNext(userDetailsService.getUserDetails(token.getUserName()));
                    emitter.onComplete();
                }
            } else {
                emitter.onError(new OauthErrorResponseException(IssuingAnAccessTokenErrorCode.INVALID_GRANT, "refresh token not found", null));
            }
        }, BackpressureStrategy.ERROR);

    }
}
