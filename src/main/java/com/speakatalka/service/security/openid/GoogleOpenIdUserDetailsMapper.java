package com.speakatalka.service.security.openid;

import com.speakatalka.model.oAuth.SocialMediaNetwork;
import com.speakatalka.service.security.UserExtractor;
import edu.umd.cs.findbugs.annotations.NonNull;
import edu.umd.cs.findbugs.annotations.Nullable;
import io.micronaut.security.authentication.AuthenticationResponse;
import io.micronaut.security.authentication.UserDetails;
import io.micronaut.security.oauth2.endpoint.authorization.state.State;
import io.micronaut.security.oauth2.endpoint.token.response.OpenIdClaims;
import io.micronaut.security.oauth2.endpoint.token.response.OpenIdTokenResponse;
import io.micronaut.security.oauth2.endpoint.token.response.OpenIdUserDetailsMapper;
import lombok.RequiredArgsConstructor;

import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
@Named("google")
@RequiredArgsConstructor
public class GoogleOpenIdUserDetailsMapper implements OpenIdUserDetailsMapper {

    private final UserExtractor userExtractor;

    //This method is deprecated and will only be called if the createAuthenticationResponse is not implemented
    @NonNull
    @Override
    public UserDetails createUserDetails(String providerName, OpenIdTokenResponse tokenResponse, OpenIdClaims openIdClaims) {
        throw new UnsupportedOperationException();
    }

    @Override
    @NonNull
    public AuthenticationResponse createAuthenticationResponse(String providerName, OpenIdTokenResponse tokenResponse, OpenIdClaims openIdClaims, @Nullable State state) {
        return userExtractor.getUserDetails(openIdClaims, SocialMediaNetwork.OPENID);
    }
}
