package com.speakatalka.service.security.fb;

import com.speakatalka.model.oAuth.FbUser;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Header;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Flowable;

@Header(name = "User-Agent", value = "micronaut")
@Client("https://graph.facebook.com")
public interface FbApiClient {

    @Get("/me?fields=${security.fb.fields}")
    Flowable<FbUser> getUser(@Header("Authorization") String authorization);
}
