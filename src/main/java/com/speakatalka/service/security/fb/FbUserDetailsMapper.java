package com.speakatalka.service.security.fb;

import com.speakatalka.model.oAuth.SocialMediaNetwork;
import com.speakatalka.service.security.UserExtractor;
import edu.umd.cs.findbugs.annotations.Nullable;
import io.micronaut.core.async.publisher.Publishers;
import io.micronaut.security.authentication.AuthenticationResponse;
import io.micronaut.security.authentication.UserDetails;
import io.micronaut.security.oauth2.endpoint.authorization.state.State;
import io.micronaut.security.oauth2.endpoint.token.response.OauthUserDetailsMapper;
import io.micronaut.security.oauth2.endpoint.token.response.TokenResponse;
import lombok.RequiredArgsConstructor;
import org.reactivestreams.Publisher;

import javax.inject.Named;
import javax.inject.Singleton;

@Named("facebook")
@Singleton
@RequiredArgsConstructor
public class FbUserDetailsMapper implements OauthUserDetailsMapper {
    private final FbApiClient apiClient;
    private final UserExtractor userExtractor;

    @Override
    public Publisher<UserDetails> createUserDetails(TokenResponse tokenResponse) {
        return Publishers.just(new UnsupportedOperationException());
    }

    @Override
    public Publisher<AuthenticationResponse> createAuthenticationResponse(TokenResponse tokenResponse, @Nullable State state) {
        return apiClient.getUser("Bearer " + tokenResponse.getAccessToken())
                .map(user -> userExtractor.getUserDetails(user, SocialMediaNetwork.FB));
    }

}
