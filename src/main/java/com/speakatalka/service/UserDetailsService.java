package com.speakatalka.service;

import com.speakatalka.entity.User;
import com.speakatalka.entity.UserRolesEntity;
import com.speakatalka.repository.UserRepository;
import com.speakatalka.repository.UserRolesRepository;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import io.micronaut.security.authentication.UserDetails;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import org.mindrot.jbcrypt.BCrypt;

import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Singleton
@Builder
@RequiredArgsConstructor
public class UserDetailsService {

    private final UserRepository userRepository;
    private final UserRolesRepository userRolesRepository;

    public UserDetails getUserDetails(String userName) {
        var userRecord = userRepository.findByName(userName).orElseThrow(() -> new HttpStatusException(HttpStatus.FORBIDDEN, "Пользователь с таким именем не найден"));
        var userRoles = userRolesRepository.findAllByUserId(userRecord.getId()).stream()
                .map(UserRolesEntity::getRoleName)
                .collect(Collectors.toList());
        return new UserDetails(userRecord.getName(), userRoles, getAttributes(userRecord));
    }

    public UserDetails getUserDetails(String userName, String password) {
        var userRecord = userRepository.findByName(userName).orElseThrow(() -> new HttpStatusException(HttpStatus.FORBIDDEN, "Пользователь с таким именем не найден"));
        var userRoles = userRolesRepository.findAllByUserId(userRecord.getId()).stream()
                .map(UserRolesEntity::getRoleName)
                .collect(Collectors.toList());
        if (BCrypt.checkpw(password, userRecord.getPassword()))
            return new UserDetails(userRecord.getName(), userRoles, getAttributes(userRecord));
        throw new HttpStatusException(HttpStatus.FORBIDDEN, "Неверный пароль");

    }

    private Map<String, Object> getAttributes(User user){
        var attributes = new HashMap<String, Object>();
        attributes.put("userFio", user.getFio());
        attributes.put("userEmail", user.getEmail());
        attributes.put("id", user.getId());
        return attributes;
    }
}
