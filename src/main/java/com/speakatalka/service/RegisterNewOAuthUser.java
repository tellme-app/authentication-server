package com.speakatalka.service;

import com.speakatalka.entity.User;
import com.speakatalka.entity.UserRolesEntity;
import com.speakatalka.entity.UserSocialMedia;
import com.speakatalka.model.oAuth.OAuthUser;
import com.speakatalka.model.oAuth.SocialMediaNetwork;
import com.speakatalka.repository.UserRepository;
import com.speakatalka.repository.UserRolesRepository;
import com.speakatalka.repository.UserSocialMediaRepository;
import lombok.Builder;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import javax.transaction.Transactional;

@Singleton
@RequiredArgsConstructor
@Builder
public class RegisterNewOAuthUser {

    private final UserRepository userRepository;
    private final UserRolesRepository userRolesRepository;
    private final UserSocialMediaRepository userSocialMediaRepository;

    @Transactional
    public User register(OAuthUser user, SocialMediaNetwork smn) {
        var newUser = User.builder()
                .email(user.getEmail())
                .fio(user.getName())
                .name(user.getEmail())
                .build();
        var savedUser = userRepository.save(newUser);
        var newUserSocial = UserSocialMedia.builder()
                .socialMediaName(smn.name())
                .userId(savedUser.getId())
                .socialMediaId(user.getId())
                .build();
        userSocialMediaRepository.save(newUserSocial);
        userRolesRepository.save(UserRolesEntity.builder().userId(savedUser.getId()).roleName("ROLE_USER").build());
        return savedUser;
    }
}
