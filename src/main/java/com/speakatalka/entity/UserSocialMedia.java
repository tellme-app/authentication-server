package com.speakatalka.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(schema = "main", name = "user_social_media")
public class UserSocialMedia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_social_media_record_id")
    private Integer id;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "social_media_name")
    private String socialMediaName;

    @Column(name = "social_media_id")
    private String socialMediaId;

}
