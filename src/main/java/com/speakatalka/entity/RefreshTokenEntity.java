package com.speakatalka.entity;

import edu.umd.cs.findbugs.annotations.NonNull;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.data.annotation.DateCreated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Introspected
@Table(schema = "main", name = "refresh_token")
public class RefreshTokenEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "refresh_token_id")
    private Long id;

    @NonNull
    @NotBlank
    @Column(name = "username")
    private String userName;

    @NonNull
    @NotBlank
    @Column(name = "refresh_token_value")
    private String refreshToken;

    @DateCreated
//    @NonNull
//    @NotBlank
    @Column(name = "date_created")
    private Instant dateCreated;

    @NonNull
    @NotBlank
    @Column(name = "revoked")
    private Boolean isRevoked;

}
