package com.speakatalka.model.oAuth;

public interface OAuthUser {

    String getId();
    String getEmail();
    String getName();
}
