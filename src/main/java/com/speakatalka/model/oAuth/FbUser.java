package com.speakatalka.model.oAuth;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Introspected
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FbUser implements OAuthUser{
    private String id;
    private String email;
    private String name;
}
