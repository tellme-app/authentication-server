package com.speakatalka.model.oAuth;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Introspected
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OpenIdUser implements OAuthUser{

    private String id;
    private String email;
    private String name;
}
