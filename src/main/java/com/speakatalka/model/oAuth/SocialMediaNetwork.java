package com.speakatalka.model.oAuth;

public enum SocialMediaNetwork {
    FB, OPENID
}
