package com.speakatalka.model;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Introspected
public class RegisterNewUserModel {

    @NotNull @NotBlank
    private String name;
    @NotNull @NotBlank
    private String fio;
    @NotNull @NotBlank
    private String email;
    @NotNull @NotBlank
    private String password;
    @NotNull @NotBlank
    private String confirmPassword;
}
