package com.speakatalka;

import io.micronaut.openapi.annotation.OpenAPIManagement;
import io.micronaut.openapi.annotation.OpenAPISecurity;
import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.tags.Tag;

@OpenAPIDefinition(
        info = @Info(
                title = "Authentication Server API",
                version = "0.0",
                description = "Описание API сервера для аутентификации",
                license = @License(name = "Apache 2.0", url = "https://speakatalka.com"),
                contact = @Contact(url = "https://speakatalka.com", name = "SpeakaTalka", email = "team@speakatalka.com")
        )
)

@OpenAPISecurity(tags = @Tag(name = "Security"))
@OpenAPIManagement(tags = @Tag(name = "Management"))
public class Application {

    public static void main(String[] args) {
        Micronaut.run(Application.class);
    }
}
