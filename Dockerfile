FROM openjdk:15-alpine as build
WORKDIR /app
COPY . /app
RUN ./gradlew build -x test

FROM openjdk:15-alpine
COPY --from=build /app/build/layers/libs /home/app/libs
COPY --from=build /app/build/layers/resources /home/app/resources
COPY --from=build /app/build/layers/application.jar /home/app/application.jar
EXPOSE 8080
EXPOSE 8443
CMD ["java", "-Dcom.sun.management.jmxremote", "-Xmx128m", "-jar", "/home/app/application.jar"]
